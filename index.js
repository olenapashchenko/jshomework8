/* 1.Опишіть своїми словами що таке Document Object Model (DOM).
Об'єктна модель документа, яка представляє весь вміст сторінки у вигляді об'єктів,
які можна міняти.

2. Яка різниця між властивостями HTML-елементів innerHTML та innerText?
innerText показує текст, який не відноситься до синтаксису HTML. innerHTML показує
і текст і розмітку HTML документу.

3.Як можна звернутися до елемента сторінки за допомогою JS? Який спосіб кращий?
До елемента сторінки можна звернутися за допомогою методу getElementById, але
краще його не використовувати, адже він звертається до глобальної змінної, що може
привести до виникнення помилок. Краще використовувати querySelector.
*/

/////////////////////////////////////////////////////////////////////////

// 1.Знайти всі параграфи на сторінці та встановити колір фону #ff0000

let p = document.querySelectorAll('p');
console.log(p);
for (let elem of p){
elem.style.backgroundColor = "#ff0000";
}

// 2.Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та 
// вивести в консоль. Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.

let optionsList = document.querySelector('#optionsList');
console.log(optionsList);

let parentElem = optionsList.parentElement;
console.log(parentElem);

let childElem = optionsList.children;
for (let child of childElem){
    console.log(child);
}

// 3. Встановіть в якості контента елемента з класом testParagraph наступний параграф -
// 'This is a paragraph'

let p2 = document.createElement('p');
p2.className = "testParagraph";
p2.innerHTML = "This is a paragraph";

document.body.append(p2);

// 4. Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. 
// Кожному з елементів присвоїти новий клас nav-item.

let elements = document.querySelector('.main-header').children;
console.log(elements);

for (elem of elements){
    elem.classList.add("nav-item");
        console.log(elem);
}

// 5. Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.

let elements2 = document.querySelectorAll('.section-title');
console.log(elements2);

for (let elem of elements2){
    elem.classList.remove('section-title');
    console.log(elem);
}

// інший варіант

// elements2.forEach(elem => {
//     elem.classList.remove('section-title');
//     console.log(elem);
// }) 